import postgres from 'postgres';
import { RecordWithWMS, SkuBatchUpdate } from '../interfaces.util';

// In a real world application I would surround this class with an integration test to validate its behavior
// This library does not appear trivial to unit test, so I am going to skip that for now
/**
 * Class intended to act as the interface between business logic and database. Domain objects come in and this service
 * class converts them to what the postgres client expects and runs the query.
 */
export class DatabaseService {
  private readonly sql: postgres.Sql;

  constructor(connectionString: string) {
    this.sql = postgres(connectionString);
  }

  async insertNewInventory(insertRecords: RecordWithWMS[]): Promise<number> {
    const valuesToInsert = insertRecords.map(record => ([record.skuId, record.skuBatchId]));
    const result = await this.sql`INSERT INTO test_table (col_1, col_2) values ${this.sql(valuesToInsert)}`;
    return result.length;
  }

  async updateInventory(updates: SkuBatchUpdate[]): Promise<number> {
    return this._processUpdates('inventory', updates);
  }

  async updateInventoryAggregate(updates: SkuBatchUpdate[]): Promise<number> {
    return this._processUpdates('inventory_aggregate', updates);
  }

  async _processUpdates(table: string, updates: SkuBatchUpdate[]): Promise<number> {
    const valuesToUpdate = updates
      .flatMap(update => update.updates
        .map(updateRecord => ({field: updateRecord.field, value: updateRecord.newValue, skuBatchId: update.skuBatchId})));

    const results = await Promise.all(valuesToUpdate.map(updateRecord =>
      this.sql`UPDATE ${this.sql(table)} SET ${this.sql(updateRecord.field)} = ${updateRecord.value} WHERE sku_batch_id = ${updateRecord.skuBatchId}`
    ));

    return results
      .map(row => row.length)
      .filter(resultCount => resultCount != 0)
      .reduce((acc, curr) => acc + curr);
  }
}