import { snakeCase } from 'lodash';
import {
  InventoryUpdate,
  RecordWithWMS,
  SkuBatchData,
  SkuBatchToSkuId,
  SkuBatchUpdate,
  WMSWarehouseMeta,
} from './interfaces.util';
import {
  appData,
  appSkuBatchData,
  appSkuBatchDataForSkuBatchIds,
  skuBatchIdsFromAppDb,
  skuBatchIdsFromInventoryDb,
  warehouseData
} from "./db/data";
import { DatabaseService } from './db/DatabaseService';

const logger = console;

/**
 * Create a list of records for a skuBatch record that maps skuBatchId + warehouseId
 * @param skuBatchRecord
 */
const makeWarehouseRecordsForSkuBatchRecord = (skuBatchRecord: SkuBatchToSkuId): RecordWithWMS[] => {
  return warehouseData.map(
    (warehouse: WMSWarehouseMeta): RecordWithWMS => ({
      skuBatchId: skuBatchRecord.skuBatchId,
      skuId: skuBatchRecord.skuId,
      wmsId: skuBatchRecord.wmsId,
      quantityPerUnitOfMeasure: skuBatchRecord.quantityPerUnitOfMeasure ?? 1,
      isArchived: skuBatchRecord.isArchived,
      isDeleted: skuBatchRecord.isDeleted,
      warehouseId: warehouse.warehouseId,
    }),
  );
};

function filterMissingRecords<T>(record: T | undefined): record is T {
  return record != undefined;
}

const findMatchingDBSkuBatchRecord = (skuBatchId: string): SkuBatchToSkuId | undefined => {
  const dbRecord = appData.find(
    (skuBatchToSkuId: SkuBatchToSkuId): boolean => skuBatchToSkuId.skuBatchId === skuBatchId,
  );

  if (dbRecord == undefined) {
    logger.error(`no records found in app SkuBatch [skuBatchId=${skuBatchId}}]`);
  }

  return dbRecord;
}

const findMatchingInventoryRecord = (appRecord: SkuBatchData, inventorySkuBatchData: SkuBatchData[]): [SkuBatchData, SkuBatchData | undefined] => {
  const matchingRecord = inventorySkuBatchData
    .find((inventoryRecord: SkuBatchData): boolean => appRecord.skuBatchId === inventoryRecord.skuBatchId);

  if (!matchingRecord) {
    logger.warn(`cannot find matching inventory record! [skuBatchId=${appRecord.skuBatchId}`);
  }

  return [appRecord, matchingRecord];
}

const processRecordsForUpdates = (recordPair: [SkuBatchData, SkuBatchData | undefined]): SkuBatchUpdate | undefined => {
  const diffRecords = (appRecord: SkuBatchData, inventoryRecord: SkuBatchData): InventoryUpdate[] => Object.entries(appRecord)
    .filter(([key, _]) => !(key === 'skuBatchId'))
    .map(([key, appValue]) => {
      const inventoryValue = inventoryRecord[key as keyof typeof inventoryRecord];

      if (key == 'skuId' && inventoryValue !== null) {
        return undefined;
      }

      if (inventoryValue != appValue) return ({ field: key, newValue: appValue });

      return undefined;
    })
    .filter(filterMissingRecords);

  const [appRecord, inventoryRecord] = recordPair;
  if (inventoryRecord === undefined) return undefined;

  const updates = diffRecords(appRecord, inventoryRecord);

  return updates.length > 0 ? {
    skuBatchId: inventoryRecord.skuBatchId,
    updates
  } : undefined;
}

/**
 * Converts a list of skuBatchIds from the app db into an insert to inventory.
 * @param skuBatchIdsToInsert
 */
export async function skuBatchToInserts(skuBatchIdsToInsert: string[]): Promise<RecordWithWMS[]> {

  const matchingSkuBatchRecords = skuBatchIdsToInsert
    .map(findMatchingDBSkuBatchRecord);

  const badRecords = matchingSkuBatchRecords.filter(record => record == undefined);

  const inserts = matchingSkuBatchRecords
    .filter(filterMissingRecords)
    .flatMap(makeWarehouseRecordsForSkuBatchRecord);

  logger.log(`processed records to insert badSkuBatchRecordCount=${badRecords.length}]`);

  return inserts;
}

/**
 * Diffs the inventory between app SkuBatch and inventory to determine
 * what we need to copy over.
 */
export async function getDeltas(): Promise<string[]> {
  try {
    const inventorySkuBatchIds: Set<string> = new Set<string>(skuBatchIdsFromInventoryDb
      .map((r: { skuBatchId: string }) => r.skuBatchId));
    return [...new Set<string>(skuBatchIdsFromAppDb.map((r: { id: string }) => r.id))]
      .filter((x: string) => !inventorySkuBatchIds.has(x));
  } catch (err) {
    logger.error('error querying databases for skuBatchIds');
    logger.error(err);
    throw err;
  }
}

/**
 * Finds the deltas between two lists of SkuBatchData
 * @param appSkuBatchData
 * @param inventorySkuBatchData
 */
export const findDeltas = (
  appSkuBatchData: SkuBatchData[],
  inventorySkuBatchData: SkuBatchData[],
): SkuBatchUpdate[] => {
  logger.log('finding data changes between inventory and app SkuBatch datasets');

  return appSkuBatchData
    .map(appRecord => findMatchingInventoryRecord(appRecord, inventorySkuBatchData))
    .map(processRecordsForUpdates)
    .filter(filterMissingRecords);
};

async function getLogisticsSkuBatchesForInventoryData(inventoryData: SkuBatchData[]) {
  const inventorySkuBatchIds = inventoryData.map(record => record.skuBatchId);
  logger.log(`querying Logistics.SkuBatch for data [skuBatchIdCount=${inventorySkuBatchIds.length}]`);
  return Promise.resolve(appSkuBatchDataForSkuBatchIds);
}

/**
 * Finds changes in data between the app SkuBatch+Sku and inventory tables
 */
export async function findChangesBetweenDatasets(): Promise<SkuBatchUpdate[]> {
  logger.log('finding app SkuBatch data that has changed and <> the inventory data');

  const inventoryData = appSkuBatchData;
  const appData = await getLogisticsSkuBatchesForInventoryData(inventoryData);

  if (appData.length != inventoryData.length) {
    // I had this logic flipped based on the comment but the data didn't support that happening, so I switched it around

    // implement the logic to log a message with the IDs missing from app
    // data that exist in the inventory data
    const appIds = appData.map(appDataRecord => appDataRecord.skuBatchId);
    const inventoryIds = inventoryData.map(inventoryDataRecord => inventoryDataRecord.skuBatchId);
    const missingIds = appIds.filter(id => !inventoryIds.includes(id));
    logger.warn(`found app items that don't exist in the inventory data: ${missingIds.join(', ')}`);
  }

  const requiredUpdates = findDeltas(appData, inventoryData);

  logger.log(`built updates [count=${requiredUpdates.length}]`);

  return requiredUpdates;
}

/**
 * Updates inventory data from app SkuBatch and Sku
 */
export async function copyMissingInventoryRecordsFromSkuBatch(databaseService: DatabaseService): Promise<void | Error> {
  logger.log('copying missing inventory records from app Sku/SkuBatch');

  // find out what skuBatchIds don't exist in inventory
  const skuBatchIdsToInsert: string[] = await getDeltas();
  logger.log(`copying new skuBatch records... [skuBatchCount=${skuBatchIdsToInsert.length}]`);
  try {
    const inserts = await skuBatchToInserts(skuBatchIdsToInsert);
    const insertCount = await databaseService.insertNewInventory(inserts);
    logger.log(`inserted new inventory - insert count: ${insertCount}`);
  } catch (err) {
    logger.error(err);
    throw err;
  }

  logger.log('done updating additive data to inventory from app db');
}

/**
 * Pulls inventory and SkuBatch data and finds changes in SkuBatch data
 * that are not in the inventory data.
 */
export async function updateInventoryDeltasFromSkuBatch(databaseService: DatabaseService): Promise<void> {
  logger.log('updating inventory from deltas in "SkuBatch" data');

  try {
    const updates: SkuBatchUpdate[] = await findChangesBetweenDatasets();
    const inventoryUpdateCount = await databaseService.updateInventory(updates);
    const aggregateUpdateCount = await databaseService.updateInventoryAggregate(updates);
    logger.log(`updated ${inventoryUpdateCount} inventory records and ${aggregateUpdateCount} aggregate records`);
  } catch (err) {
    logger.error(err);
    throw err;
  }

  logger.log('done updating inventory from deltas from app db');
}

/**
 * Primary entry point to sync SkuBatch data from the app
 * database over to the inventory database
 */
export async function sync(): Promise<void | Error> {
  try {
    const postgresService = new DatabaseService('connString');
    await copyMissingInventoryRecordsFromSkuBatch(postgresService);
    await updateInventoryDeltasFromSkuBatch(postgresService);
  } catch (err) {
    logger.error('error syncing skuBatch data');
    return Promise.reject(err);
  }
}