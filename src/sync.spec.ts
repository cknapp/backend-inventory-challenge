import {
  copyMissingInventoryRecordsFromSkuBatch,
  findChangesBetweenDatasets,
  findDeltas,
  getDeltas,
  skuBatchToInserts, updateInventoryDeltasFromSkuBatch,
} from './sync';
import { SkuBatchUpdate } from "./interfaces.util";
import { DatabaseService } from './db/DatabaseService';

describe('sync', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  const mockDatabaseService = {
    insertNewInventory: jest.fn(),
    updateInventory: jest.fn(),
    updateInventoryAggregate: jest.fn()
  } as any;

  describe('.skuBatchToInserts', () => {
    it('should return a list of records to insert', async () => {
      const data = [
        {
          skuBatchId: 'sku-batch-id-1',
          skuId: 'sku-id-1',
          quantityPerUnitOfMeasure: 1,
        },
        {
          skuBatchId: 'sku-batch-id-2',
          skuId: 'sku-id-1',
          quantityPerUnitOfMeasure: 1,
        },
        {
          skuBatchId: 'sku-batch-id-3',
          skuId: 'sku-id-1',
          quantityPerUnitOfMeasure: 1,
        },
      ];

      await expect(
        skuBatchToInserts(
          data.map((d) => d.skuBatchId),
        ),
      ).resolves.toStrictEqual([{
        skuBatchId: 'sku-batch-id-1',
        skuId: 'sku-id-1',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1234,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-1',
      }, {
        skuBatchId: 'sku-batch-id-1',
        skuId: 'sku-id-1',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1234,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-2',
      }, {
        skuBatchId: 'sku-batch-id-1',
        skuId: 'sku-id-1',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1234,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-3',
      }, {
        skuBatchId: 'sku-batch-id-1',
        skuId: 'sku-id-1',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1234,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-4',
      }, {
        skuBatchId: 'sku-batch-id-2',
        skuId: 'sku-id-1',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1235,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-1',
      }, {
        skuBatchId: 'sku-batch-id-2',
        skuId: 'sku-id-1',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1235,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-2',
      }, {
        skuBatchId: 'sku-batch-id-2',
        skuId: 'sku-id-1',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1235,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-3',
      }, {
        skuBatchId: 'sku-batch-id-2',
        skuId: 'sku-id-1',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1235,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-4',
      }, {
        skuBatchId: 'sku-batch-id-3',
        skuId: 'sku-id-1',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1236,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-1',
      }, {
        skuBatchId: 'sku-batch-id-3',
        skuId: 'sku-id-1',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1236,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-2',
      }, {
        skuBatchId: 'sku-batch-id-3',
        skuId: 'sku-id-1',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1236,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-3',
      }, {
        skuBatchId: 'sku-batch-id-3',
        skuId: 'sku-id-1',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1236,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-4',
      }
      ]);
    });
  });

  describe('.getDeltas', () => {
    it('should find deltas', async () => {
      // example of the data below:
      // skuBatchIds = ['sku-batch-id-1', 'sku-batch-id-2', 'sku-batch-id-3', 'sku-batch-id-4'];
      // appSkuBatchIds = [...skuBatchIds, 'sku-batch-id-5', 'sku-batch-id-6']; // 5 and 6 are new
      await expect(getDeltas()).resolves.toStrictEqual(['sku-batch-id-5', 'sku-batch-id-6']);
    });
  });

  describe('.findDelta', () => {
    it('should pick up changes to quantityPerUnitOfMeasure', async () => {
      const appData = [{
        skuBatchId: '1',
        skuId: '1',
        wmsId: '1',
        quantityPerUnitOfMeasure: 5,
        isArchived: false,
        isDeleted: false,
      }];

      const inventoryData = [{
        skuBatchId: '1',
        skuId: '1',
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const deltas: SkuBatchUpdate[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(1);
      expect(deltas[0].updates.length).toBe(1);
      expect(deltas[0].updates[0].field).toBe('quantityPerUnitOfMeasure');
      expect(deltas[0].updates[0].newValue).toBe(5);
    });

    it('should not change the skuId if already set', async () => {
      const appData = [{
        skuBatchId: '1',
        skuId: '3',
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const inventoryData = [{
        skuBatchId: '1',
        skuId: '1',
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const deltas: SkuBatchUpdate[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(0);
    });

    it('should pick up change to skuId if not set', async () => {
      const appData = [{
        skuBatchId: '1',
        skuId: '1',
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const inventoryData = [{
        skuBatchId: '1',
        skuId: null,
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const deltas: SkuBatchUpdate[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(1);
      expect(deltas[0].updates.length).toBe(1);
      expect(deltas[0].updates[0].field).toBe('skuId');
      expect(deltas[0].updates[0].newValue).toBe('1');
    });

    it('should pick up change to wmsId', async () => {
      const appData = [{
        skuBatchId: '1',
        skuId: '1',
        wmsId: '4',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const inventoryData = [{
        skuBatchId: '1',
        skuId: '1',
        wmsId: '1',
        quantityPerUnitOfMeasure: 10,
        isArchived: false,
        isDeleted: false,
      }];

      const deltas: SkuBatchUpdate[] = findDeltas(appData, inventoryData);
      expect(deltas.length).toBe(1);
      expect(deltas[0].updates.length).toBe(1);
      expect(deltas[0].updates[0].field).toBe('wmsId');
      expect(deltas[0].updates[0].newValue).toBe('4');
    });

    it('should find changes between datasets', async () => {
      await expect(
        findChangesBetweenDatasets(),
      ).resolves.toStrictEqual([{
        skuBatchId: 'sku-batch-id-5',
        updates: [{field: 'isDeleted', newValue: true}]
      }, {
        skuBatchId: 'sku-batch-id-6',
        updates: [{field: 'isArchived', newValue: true}]
      }]);
    });
  });

  describe('copyMissingInventoryRecordsFromSkuBatch', () => {
    it('should correctly insert records to the inventory', async () => {
      await copyMissingInventoryRecordsFromSkuBatch(mockDatabaseService);

      expect(mockDatabaseService.insertNewInventory).toHaveBeenCalledTimes(1);
      expect(mockDatabaseService.insertNewInventory).toHaveBeenCalledWith([{
        skuBatchId: 'sku-batch-id-5',
        skuId: 'sku-id-2',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1238,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-1',
      }, {
        skuBatchId: 'sku-batch-id-5',
        skuId: 'sku-id-2',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1238,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-2',
      }, {
        skuBatchId: 'sku-batch-id-5',
        skuId: 'sku-id-2',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1238,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-3',
      }, {
        skuBatchId: 'sku-batch-id-5',
        skuId: 'sku-id-2',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1238,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-4',
      }, {
        skuBatchId: 'sku-batch-id-6',
        skuId: 'sku-id-3',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1239,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-1',
      }, {
        skuBatchId: 'sku-batch-id-6',
        skuId: 'sku-id-3',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1239,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-2',
      }, {
        skuBatchId: 'sku-batch-id-6',
        skuId: 'sku-id-3',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1239,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-3',
      }, {
        skuBatchId: 'sku-batch-id-6',
        skuId: 'sku-id-3',
        quantityPerUnitOfMeasure: 1,
        wmsId: 1239,
        isArchived: false,
        isDeleted: false,
        warehouseId: 'warehouse-4',
      }]);
    });
  });

  describe('updateInventoryDeltasFromSkuBatch', () => {
    it('should correctly update records to inventory and aggregate', async () => {
      await updateInventoryDeltasFromSkuBatch(mockDatabaseService);

      const expectedArguments: SkuBatchUpdate[] = [{
        skuBatchId: 'sku-batch-id-5',
        updates: [{field: 'isDeleted', newValue: true}]
      }, {
        skuBatchId: 'sku-batch-id-6',
        updates: [{field: 'isArchived', newValue: true}]
      }];

      expect(mockDatabaseService.updateInventory).toHaveBeenCalledTimes(1);
      expect(mockDatabaseService.updateInventory).toHaveBeenCalledWith(expectedArguments);
      expect(mockDatabaseService.updateInventoryAggregate).toHaveBeenCalledTimes(1);
      expect(mockDatabaseService.updateInventoryAggregate).toHaveBeenCalledWith(expectedArguments);
    });
  });
});
